package br.edu.unisep.controledengue.domain.repository

import br.edu.unisep.controledengue.data.dao.AddressDao
import br.edu.unisep.controledengue.data.db.DengueDb
import br.edu.unisep.controledengue.data.entity.Address
import br.edu.unisep.controledengue.domain.dto.address.AddressDto
import br.edu.unisep.controledengue.domain.dto.address.NewAddressDto
import br.edu.unisep.controledengue.domain.utils.convertGrade

class AddressRepository {

    private val dao: AddressDao by lazy {
        DengueDb.instance.addressDao()
    }

    suspend fun save(newAddress: NewAddressDto) {
        val address = Address(
            newAddress.owner,
            newAddress.street,
            newAddress.number,
            newAddress.neighborhood
        )
        dao.save(address)
    }

    suspend fun findAll(): List<AddressDto> {
        val result = dao.findAll()
        return result.map { address ->
            AddressDto(
                address.id!!,
                address.owner,
                address.street,
                address.number,
                address.neighborhood,
                convertGrade(address.grade)
            )
        }
    }

}