package br.edu.unisep.controledengue.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Inspection(
    val date: String,
    val grade: Float,
    val hasLarvae: Boolean,
    val hasGarbage: Boolean,
    val hasStandingWater: Boolean,
    val hasLackOfMaintenance: Boolean,
    val addressId: Int,
) {
    @PrimaryKey(autoGenerate = true) var id: Int? = null
}
