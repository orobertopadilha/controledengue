package br.edu.unisep.controledengue.ui.inspection.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.edu.unisep.controledengue.R
import br.edu.unisep.controledengue.databinding.ActivityInspectionsBinding
import br.edu.unisep.controledengue.domain.dto.address.AddressDto
import br.edu.unisep.controledengue.domain.dto.inspection.NewInspectionDto
import br.edu.unisep.controledengue.ui.inspection.list.adapter.InspectionsAdapter
import br.edu.unisep.controledengue.ui.inspection.list.contract.ListInspectionsContract
import br.edu.unisep.controledengue.ui.inspection.list.viewmodel.ListInspectionsViewModel
import br.edu.unisep.controledengue.ui.inspection.register.RegisterInspectionDialog

class InspectionsActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityInspectionsBinding.inflate(layoutInflater)
    }

    private val viewModel: ListInspectionsViewModel by viewModels()

    private lateinit var adapter: InspectionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
        setupObservers()

        viewModel.findAll()
    }

    private fun setupView() {
        viewModel.address = intent.getSerializableExtra("address") as AddressDto

        adapter = InspectionsAdapter()

        binding.rvInspections.adapter = adapter
        binding.rvInspections.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvInspections.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
    }

    private fun setupObservers() {
        viewModel.inspections.observe(this) {
            adapter.inspections = it
        }

        viewModel.saveResult.observe(this) {
            viewModel.findAll()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnNew) {
            RegisterInspectionDialog.createAndShow(supportFragmentManager, ::onSave)
            return true
        }

        return false
    }

    private fun onSave(inspection: NewInspectionDto) {
        viewModel.save(inspection)
    }

    override fun onBackPressed() {
        if (viewModel.hasSaved) {
            setResult(ListInspectionsContract.RESULT_INSPECTION_CREATED)
            finish()
        } else {
            super.onBackPressed()
        }
    }
}