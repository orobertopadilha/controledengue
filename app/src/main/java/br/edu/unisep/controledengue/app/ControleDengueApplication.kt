package br.edu.unisep.controledengue.app

import android.app.Application
import br.edu.unisep.controledengue.data.db.DengueDb

class ControleDengueApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DengueDb.initialize(this)
    }
}