package br.edu.unisep.controledengue.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Address(
    val owner: String,
    val street: String,
    val number: String?,
    val neighborhood: String,
    val grade: Float? = null
) {
    @PrimaryKey(autoGenerate = true) var id: Int? = null
}
