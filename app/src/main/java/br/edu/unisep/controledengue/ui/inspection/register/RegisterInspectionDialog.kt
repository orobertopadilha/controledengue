package br.edu.unisep.controledengue.ui.inspection.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import br.edu.unisep.controledengue.databinding.DialogNewInspectionBinding
import br.edu.unisep.controledengue.domain.dto.inspection.NewInspectionDto
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

typealias OnSaveInspection = (NewInspectionDto) -> Unit

class RegisterInspectionDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogNewInspectionBinding
    lateinit var onSave: OnSaveInspection

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogNewInspectionBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.btnSave.setOnClickListener { save() }
    }

    private fun save() {
        val inspection = NewInspectionDto(
            binding.swLarvae.isChecked,
            binding.swGarbage.isChecked,
            binding.swWater.isChecked,
            binding.swMaintenance.isChecked
        )

        onSave(inspection)
        dialog?.dismiss()
    }

    companion object {

        @JvmStatic
        fun createAndShow(fragmentManager: FragmentManager, onSave: OnSaveInspection) {
            val dialog = RegisterInspectionDialog()
            dialog.onSave = onSave
            dialog.show(fragmentManager, "register-inspection")
        }
    }
}