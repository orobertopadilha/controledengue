package br.edu.unisep.controledengue.ui.inspection.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.controledengue.databinding.ItemInspectionBinding
import br.edu.unisep.controledengue.domain.dto.inspection.InspectionDto
import br.edu.unisep.controledengue.domain.utils.getDetails

class InspectionsAdapter: RecyclerView.Adapter<InspectionsAdapter.InspectionsViewHolder>() {

    var inspections: List<InspectionDto> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InspectionsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemInspectionBinding.inflate(layoutInflater, parent, false)
        return InspectionsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InspectionsViewHolder, position: Int) {
        holder.bind(inspections[position])
    }

    override fun getItemCount() = inspections.size

    class InspectionsViewHolder(private val binding: ItemInspectionBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(inspection: InspectionDto) {
            binding.tvInspectionDate.text = inspection.date
            binding.tvInspectionDetails.text = getDetails(binding.root.context, inspection)
            binding.tvInspectionGrade.text = inspection.grade
        }
    }
}