package br.edu.unisep.controledengue.ui.address.register.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.controledengue.domain.dto.address.NewAddressDto
import br.edu.unisep.controledengue.domain.repository.AddressRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewAddressViewModel : ViewModel() {

    private val repository = AddressRepository()

    private val _saveResult = MutableLiveData<Unit>()
    val saveResult: LiveData<Unit>
        get() = _saveResult

    fun save(address: NewAddressDto) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.save(address)
            _saveResult.postValue(Unit)
        }
    }
}