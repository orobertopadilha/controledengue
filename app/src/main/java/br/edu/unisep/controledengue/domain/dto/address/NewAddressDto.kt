package br.edu.unisep.controledengue.domain.dto.address

data class NewAddressDto(
    val owner: String,
    val street: String,
    val number: String?,
    val neighborhood: String,
)
