package br.edu.unisep.controledengue.ui.address.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import br.edu.unisep.controledengue.R
import br.edu.unisep.controledengue.databinding.ActivityNewAddressBinding
import br.edu.unisep.controledengue.domain.dto.address.NewAddressDto
import br.edu.unisep.controledengue.ui.address.register.viewmodel.NewAddressViewModel

class NewAddressActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityNewAddressBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<NewAddressViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupEvents()
    }

    private fun setupEvents() {
        binding.btnSave.setOnClickListener { save() }

        viewModel.saveResult.observe(this) {
            onSaveResult()
        }
    }

    private fun save() {
        val address = NewAddressDto(
            owner = binding.etOwner.text.toString(),
            street = binding.etStreet.text.toString(),
            number = binding.etNumber.text.toString(),
            neighborhood = binding.etNeighborhood.text.toString()
        )

        viewModel.save(address)
    }

    private fun onSaveResult() {
        setResult(RESULT_OK)
        finish()
    }

}