package br.edu.unisep.controledengue.domain.dto.inspection

data class InspectionDto(
    val id: Int,
    val date: String,
    val grade: String,
    val hasLarvae: Boolean,
    val hasGarbage: Boolean,
    val hasStandingWater: Boolean,
    val hasLackOfMaintenance: Boolean
)
