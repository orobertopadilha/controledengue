package br.edu.unisep.controledengue.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import br.edu.unisep.controledengue.data.entity.Address

@Dao
interface AddressDao {

    @Insert
    suspend fun save(address: Address)

    @Query("update address set grade = :grade where id = :addressId")
    suspend fun update(addressId: Int, grade: Float)

    @Query("select * from address")
    suspend fun findAll(): List<Address>

    @Query("select * from address where id = :id")
    suspend fun findById(id: Int): Address?

}