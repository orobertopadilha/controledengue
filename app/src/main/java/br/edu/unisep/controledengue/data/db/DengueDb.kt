package br.edu.unisep.controledengue.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.edu.unisep.controledengue.data.dao.AddressDao
import br.edu.unisep.controledengue.data.dao.InspectionDao
import br.edu.unisep.controledengue.data.entity.Address
import br.edu.unisep.controledengue.data.entity.Inspection

@Database(
    entities = [Address::class, Inspection::class],
    version = 1
)
abstract class DengueDb : RoomDatabase() {

    abstract fun addressDao() : AddressDao

    abstract fun inspectionDao() : InspectionDao

    companion object {
        private lateinit var mInstance: DengueDb
        val instance: DengueDb
            get() = mInstance

        fun initialize(context: Context) {
            mInstance = Room.databaseBuilder(context,
                DengueDb::class.java, "db_dengue").build()
        }
    }
}