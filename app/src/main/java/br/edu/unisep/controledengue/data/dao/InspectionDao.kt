package br.edu.unisep.controledengue.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import br.edu.unisep.controledengue.data.entity.Address
import br.edu.unisep.controledengue.data.entity.Inspection

@Dao
interface InspectionDao {

    @Insert
    suspend fun save(inspection: Inspection)

    @Query("select * from inspection where addressId = :addressId")
    suspend fun findAll(addressId: Int): List<Inspection>

}