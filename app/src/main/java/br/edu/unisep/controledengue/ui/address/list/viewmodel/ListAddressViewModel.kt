package br.edu.unisep.controledengue.ui.address.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.controledengue.domain.dto.address.AddressDto
import br.edu.unisep.controledengue.domain.repository.AddressRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListAddressViewModel : ViewModel(){

    private val _addresses = MutableLiveData<List<AddressDto>>()
    val addresses: LiveData<List<AddressDto>>
        get() = _addresses

    private val repository = AddressRepository()

    fun findAll() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.findAll()
            _addresses.postValue(list)
        }
    }
}