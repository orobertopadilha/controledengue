package br.edu.unisep.controledengue.domain.utils

import android.content.Context
import br.edu.unisep.controledengue.R
import br.edu.unisep.controledengue.data.entity.Inspection
import br.edu.unisep.controledengue.domain.dto.inspection.InspectionDto
import br.edu.unisep.controledengue.domain.dto.inspection.NewInspectionDto

fun convertGrade(grade: Float?) =
    when (grade?.toInt()) {
        0 -> "A"
        1 -> "B"
        2 -> "C"
        3 -> "D"
        4 -> "E"
        else -> "-"
    }

fun getGrade(inspection: NewInspectionDto): Float {
    var grade: Float = 0f

    if (inspection.hasGarbage) grade++
    if (inspection.hasLackOfMaintenance) grade++
    if (inspection.hasLarvae) grade++
    if (inspection.hasStandingWater) grade++

    return grade
}

fun getDetails(context: Context, inspection: InspectionDto): String {
    val details = mutableListOf<String>()

    if (inspection.hasGarbage)
        details += context.getString(R.string.garbage)

    if (inspection.hasLackOfMaintenance)
        details += context.getString(R.string.lackOfMaintenance)

    if (inspection.hasLarvae)
        details += context.getString(R.string.larvaeSpots)

    if (inspection.hasStandingWater)
        details += context.getString(R.string.standingWater)

    return if (details.isNotEmpty()) {
        details.joinToString()
    } else {
        context.getString(R.string.noOccurrence)
    }
}