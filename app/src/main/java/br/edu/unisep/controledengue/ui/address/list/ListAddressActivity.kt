package br.edu.unisep.controledengue.ui.address.list

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import br.edu.unisep.controledengue.R
import br.edu.unisep.controledengue.databinding.ActivityListAddressBinding
import br.edu.unisep.controledengue.domain.dto.address.AddressDto
import br.edu.unisep.controledengue.ui.address.list.adapter.AddressAdapter
import br.edu.unisep.controledengue.ui.address.list.viewmodel.ListAddressViewModel
import br.edu.unisep.controledengue.ui.address.register.NewAddressActivity
import br.edu.unisep.controledengue.ui.inspection.list.contract.ListInspectionsContract

class ListAddressActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityListAddressBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<ListAddressViewModel>()

    private lateinit var adapter: AddressAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
        setupObservers()

        viewModel.findAll()
    }

    private fun setupView() {
        adapter = AddressAdapter(::onAddressSelected)

        binding.rvAddresses.adapter = adapter
        binding.rvAddresses.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        binding.rvAddresses.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    private fun setupObservers() {
        viewModel.addresses.observe(this) {
            adapter.addresses = it
        }
    }

    private fun onAddressSelected(address: AddressDto) {
        inspectionsLauncher.launch(address)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnNew) {
            registerLauncher.launch(Intent(this, NewAddressActivity::class.java))
            return true
        }

        return false
    }

    private val registerLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) { result ->

        if (result.resultCode == RESULT_OK) {
            viewModel.findAll()
        }
    }

    private val inspectionsLauncher = registerForActivityResult(
        ListInspectionsContract()) { resultCode ->
            if (resultCode == ListInspectionsContract.RESULT_INSPECTION_CREATED) {
                viewModel.findAll()
            }
        }

}