package br.edu.unisep.controledengue.ui.address.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.controledengue.R
import br.edu.unisep.controledengue.databinding.ItemAddressBinding
import br.edu.unisep.controledengue.domain.dto.address.AddressDto

class AddressAdapter(private val onItemSelected: (AddressDto) -> Unit) : RecyclerView.Adapter<AddressAdapter.AddressViewHolder>(){

    var addresses: List<AddressDto> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemAddressBinding.inflate(layoutInflater, parent, false)
        return AddressViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(addresses[position], onItemSelected)
    }

    override fun getItemCount() = addresses.size

    class AddressViewHolder(private val itemBinding: ItemAddressBinding):
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(address: AddressDto, onItemSelected: (AddressDto) -> Unit) {
            val context = itemBinding.root.context

            itemBinding.tvHomeOwner.text = address.owner
            itemBinding.tvAddress.text = context.getString(R.string.fullAddress,
                address.street, address.number, address.neighborhood)

            itemBinding.tvGrade.text = address.grade

            itemBinding.root.setOnClickListener {
                onItemSelected(address)
            }
        }
    }
}