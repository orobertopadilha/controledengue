package br.edu.unisep.controledengue.domain.repository

import br.edu.unisep.controledengue.data.db.DengueDb
import br.edu.unisep.controledengue.data.entity.Inspection
import br.edu.unisep.controledengue.domain.dto.inspection.InspectionDto
import br.edu.unisep.controledengue.domain.dto.inspection.NewInspectionDto
import br.edu.unisep.controledengue.domain.utils.convertGrade
import br.edu.unisep.controledengue.domain.utils.getGrade
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class InspectionRepository {

    private val inspectionDao = DengueDb.instance.inspectionDao()
    private val addressDao = DengueDb.instance.addressDao()

    suspend fun findAll(addressId: Int): List<InspectionDto> {
        val inspections = inspectionDao.findAll(addressId)
        val result = inspections.map { inspection ->
            InspectionDto(inspection.id!!,
                inspection.date,
                convertGrade(inspection.grade),
                inspection.hasLarvae,
                inspection.hasGarbage,
                inspection.hasStandingWater,
                inspection.hasLackOfMaintenance
            )
        }

        return result
    }

    suspend fun save(newInspection: NewInspectionDto, addressId: Int) {
        val today = LocalDateTime.now()
        val dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")

        val inspection = Inspection(
            dateFormat.format(today),
            getGrade(newInspection),
            newInspection.hasLarvae,
            newInspection.hasGarbage,
            newInspection.hasStandingWater,
            newInspection.hasLackOfMaintenance,
            addressId
        )

        inspectionDao.save(inspection)

        val address = addressDao.findById(addressId)
        val newGrade = if (address?.grade != null) {
            (address.grade + inspection.grade) / 2
        } else {
            inspection.grade
        }
        
        addressDao.update(addressId, newGrade)
    }
}