package br.edu.unisep.controledengue.domain.dto.inspection

data class NewInspectionDto(
    val hasLarvae: Boolean,
    val hasGarbage: Boolean,
    val hasStandingWater: Boolean,
    val hasLackOfMaintenance: Boolean,
)

