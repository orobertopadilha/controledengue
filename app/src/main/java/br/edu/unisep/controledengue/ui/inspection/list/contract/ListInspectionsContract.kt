package br.edu.unisep.controledengue.ui.inspection.list.contract

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import br.edu.unisep.controledengue.domain.dto.address.AddressDto
import br.edu.unisep.controledengue.ui.inspection.list.InspectionsActivity

class ListInspectionsContract : ActivityResultContract<AddressDto, Int>() {

    override fun createIntent(context: Context, address: AddressDto) =
        Intent(context, InspectionsActivity::class.java).apply {
            putExtra("address", address)
        }

    override fun parseResult(resultCode: Int, intent: Intent?) = resultCode

    companion object {
        const val RESULT_INSPECTION_CREATED = 100
    }
}