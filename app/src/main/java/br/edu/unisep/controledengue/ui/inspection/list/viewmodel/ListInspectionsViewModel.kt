package br.edu.unisep.controledengue.ui.inspection.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.controledengue.domain.dto.address.AddressDto
import br.edu.unisep.controledengue.domain.dto.inspection.InspectionDto
import br.edu.unisep.controledengue.domain.dto.inspection.NewInspectionDto
import br.edu.unisep.controledengue.domain.repository.InspectionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListInspectionsViewModel : ViewModel(){

    private val _inspections = MutableLiveData<List<InspectionDto>>()
    val inspections: LiveData<List<InspectionDto>>
        get() = _inspections

    private val _saveResult = MutableLiveData<Unit>()
    val saveResult: LiveData<Unit>
        get() = _saveResult

    private var _hasSaved = false
    val hasSaved: Boolean
        get() = _hasSaved

    private val repository = InspectionRepository()

    lateinit var address: AddressDto

    fun findAll() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.findAll(address.id)
            _inspections.postValue(list)
        }
    }

    fun save(inspection: NewInspectionDto) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.save(inspection, address.id)
            _hasSaved = true
            _saveResult.postValue(Unit)
        }
    }
}