package br.edu.unisep.controledengue.domain.dto.address

import java.io.Serializable

data class AddressDto(
    val id: Int,
    val owner: String,
    val street: String,
    val number: String?,
    val neighborhood: String,
    val grade: String,
) : Serializable
